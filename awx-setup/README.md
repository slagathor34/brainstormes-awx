---
# Ansible AWX Setup Role
#
# This role installs and configures Ansible AWX, an open source web-based solution for managing Ansible playbooks, inventories, and scheduling.
#
# Requirements:
#   - Ansible 2.9 or later
#   - Ubuntu 18.04 or later
#
# Role Variables:
#   - awx_version: The version of AWX to install (default: 17.1.0)
#   - awx_admin_user: The username for the AWX admin user (default: admin)
#   - awx_admin_password: The password for the AWX admin user (default: password)
#
# Dependencies:
#   - community.docker
#   - community.pip
#
# Example Playbook:
#   - hosts: awx
#     roles:
#       - awx.awx
#
# License: MIT
# Author: Sonny Stormes (sonny@stormesfamily.com)
#
# For more information, see https://gitlab.com/slagathor34/brainstormes-awx
#