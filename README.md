# brainstormes-awx

#Start your K8 cluster with KVM2 drivers
```
minikube start --driver=kvm2 --addons=ingress --cpus=8 --install-addons=true --kubernetes-version=stable --memory=16g
```

#Create K8 namespace
```
kubectl create namespace awx
```

#Deploy Helm chart operator for awx
```
kubectl apply -k .
```
#Create bearer token
```
kubectl create token awx-api --duration 9999h -n awx
```

#Get password from K8 secrets vault 
```
kubectl get secrets -o jsonpath="{.items[?(@.metadata.annotations['kubernetes\.io/service-account\.name']=='default')].data.token}"|base64 -d
```
